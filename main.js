// TOGGLE MENU NAVBAR
var navigation = document.querySelector(".nav__wrapper__right-lists");
var navigationResponsive = document.querySelector(
  ".nav__wrapper__right-list-responsive"
);
var openMenu = document.querySelector(".menu");

openMenu.addEventListener("click", () => {
  navigationResponsive.classList.toggle("active");
});


